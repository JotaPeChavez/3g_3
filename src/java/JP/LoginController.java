/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JP;

import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
//import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

@Named(value = "loginController")
@SessionScoped
public class LoginController implements Serializable {

    public LoginController() {
    }
    private String username;
    private String pass;
    private boolean loggedIn;

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
    @EJB
    private JP.UsersFacade ejbFacade;

    public UsersFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(UsersFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public String loginControl() {
        FacesContext context = FacesContext.getCurrentInstance();
        RequestContext.getCurrentInstance().update("growl");
        if (getEjbFacade().connect(username, pass)) {
            this.setUsername(username);
            context.getExternalContext().getSessionMap().put("user", username);
            loggedIn = true;
            return "producto/Create.xhtml?faces-redirect=true";
        }
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error de acceso"));
        return "";
    }

    public void logout() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().invalidateSession();
        try {
            context.getExternalContext().redirect("../index.xhtml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

}
