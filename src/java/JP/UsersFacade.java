/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JP;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class UsersFacade extends AbstractFacade<Users> {
    
    public Users connect = null;

    @PersistenceContext(unitName = "3G_3PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public boolean connect(String username, String pass) {
              try {
            connect = this.getEntityManager().createNamedQuery("Users.control", Users.class).setParameter("username", username).setParameter("pass", pass).getSingleResult();
            if(connect != null) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
    
       public UsersFacade() {
        super(Users.class);
    }
}
